#!/bin/bash

# about:      mkspectrograms.sh -- make spectrograms with SoX, using multiprocess operation
# version:    0.2
# depends:    bash, sox, basename, dirname
# recommends: bash v4.3+ (earlier versions are also supported)
# suggests:   env_parallel (GNU Parallel) available as an optional multiprocess fall-back
# usage:      $> mkspectrograms.sh [OPTION [ARGUMENT]]... [--] SOURCE_FILE_OR_FOLDER [SOURCE_FILE_OR_FOLDER]...

  ####################
### DEFAULT SETTINGS ###

## maximum number of simultaneous jobs
jobs="0" # set "0" to attempt automatic detection of all available threads

## SoX spectrogram commands
full_spectrogram_command="-n remix 1 spectrogram -x 3000 -y 513 -z 120 -w Kaiser"
zoom_spectrogram_command="-n remix 1 spectrogram -x 500 -y 1025 -z 120 -w Kaiser" # -S 1:00 -d 0:02 # -S: start time, -d: duration

## zoomed spectrogram settings
zoom_start="3" # divisor used against track duration to set starting position of zoomed spectrograms
zoom_total="2" # length of zoomed spectrograms in seconds

## spectrogram title length limit
# (mostly just zoomed) titles may grow too long to fit within the width of a spectrogram, and then SoX will
# just omit them entirely. So counting backwards from the end, cut longer titles down to this many characters
full_title_length_limit=""
zoom_title_length_limit="85"  # leave unset ( eg: _limit="" -NOT- _limit="0" ) to ignore either limit

## Homebrew binaries folder, for running under Automator on MacOS.
homebrew_bin="" # leave unset unless you installed Homebrew in a non-default location - the script will abort if set incorrectly/unnecessarily

## SoX spectrogram commands - DEFAULTS
#full_spectrogram_command="-n remix 1 spectrogram -x 3000 -y 513 -z 120 -w Kaiser"
#zoom_spectrogram_command="-n remix 1 spectrogram -x 500 -y 1025 -z 120 -w Kaiser" # -S 1:00 -d 0:02 # -S: start time, -d: duration

### DEFAULT SETTINGS ###
  ####################


## functions ##
_error () { printf >&2 '%sERROR%s: %s\n' $'\033[0;31m' $'\033[0m' "$@" ; }

_help () {
	printf '
Name:
   mkspectrograms.sh - Make spectrograms from FLAC, WAV, or MP3 files.

Usage:
   %s [OPTION [ARGUMENT]]... [--] FLAC_WAV_MP3_OR_FOLDER [FLAC_WAV_MP3_OR_FOLDER]...

Options:
   -h, -H, --help              Print this help text.
   --                          End of options. Subsequent arguments treated as potential source files or directories.

   -j ARG, --jobs ARG          Set the maximum number of concurrent jobs to "ARG".
                               Use "0" to attempt using all available threads.

   -p, --parallel              Force use of GNU Parallel (env_parallel) for multiprocess operation.
                               Not required, now defaulting to the "wait" Bash-builtin.

   -F,     --full-only         Only create full sized spectrograms.
   -Z,     --zoom-only         Only created zoomed spectrograms.

   -f ARG, --full-title ARG    Set full spectrogram title length limit to "ARG".
   -z ARG, --zoom-title ARG    Set zoom spectrogram title length limit to "ARG".

   -s ARG, --zoom-start ARG    Set the zoomed-spectrogram-start-point-divisor to "ARG".
   -l ARG, --zoom-length ARG   Set the length in seconds of zoomed spectrograms to "ARG".

' "${0##*/}"
}

_spectrograms () { # takes -one- array index as argument
	# output dir
	[[ ! -d ${absolute_source_dirs[$1]}/Spectrograms/ ]] &&
		if ! mkdir -p "${absolute_source_dirs[$1]}"/Spectrograms/ ;then
			_error "Failure creating output folder '${absolute_source_dirs[$1]##*/}/Spectrograms/'."
			_error "Aborting spectrogram creation for ${source_filenames[$1]}."
			return 1
		fi

	local spectrogram_title="${absolute_source_dirs[$1]##*/}/${source_filenames[$1]}"

	# full spectrogram
	[[ $zoom_only != "1" ]] && {
		[[ -n $full_title_length_limit && ${#spectrogram_title} -gt $full_title_length_limit ]] && spectrogram_title="... ${spectrogram_title: -$full_title_length_limit}"
		if ! sox "${absolute_sources[$1]}" $full_spectrogram_command \
			 -t "$spectrogram_title" \
			 -c "         ${source_bit_depths[$1]} bit  |  ${source_sample_rates[$1]} Hz  |  ${source_durations[$1]%.*} sec" \
			 -o "${absolute_source_dirs[$1]}"/Spectrograms/"${source_filenames[$1]}"-full.png # just leave source file's extension in image filename for now / until better idea
		then                                             # "${source_filenames[$1]%.[Ff][Ll][Aa][Cc]}"-full.png # or file1.mp3's spectrogram might overwrite file1.flac's or file1.wav's
			_error "Failure creating full spectrogram for '${absolute_sources[$1]}'."
			local fail="1"
		fi
	}

	# zoomed spectrogram
	[[ $full_only != "1" ]] && {
		local start_zoom="$(( ${source_durations[$1]%.*} / $zoom_start ))"
		local zoom_spectrogram_command="$zoom_spectrogram_command -S 0:$start_zoom -d 0:$zoom_total"

		[[ -n $zoom_title_length_limit && ${#spectrogram_title} -gt $zoom_title_length_limit ]] && spectrogram_title="... ${spectrogram_title: -$zoom_title_length_limit}"
		if ! sox "${absolute_sources[$1]}" $zoom_spectrogram_command \
			 -t "$spectrogram_title" \
			 -c "         ${source_bit_depths[$1]} bit  |  ${source_sample_rates[$1]} Hz  |  $zoom_total sec  |  starting @ $start_zoom sec" \
			 -o "${absolute_source_dirs[$1]}"/Spectrograms/"${source_filenames[$1]}"-zoom.png # "${source_filenames[$1]%.[Ff][Ll][Aa][Cc]}"-zoom.png
		then
			_error "Failure creating zoomed spectrogram for '${absolute_sources[$1]}'."
			local fail="1"
		fi
	}

	[[ $fail -eq "1" ]] && return 1
	return 0
}


[ -n "$BASH_VERSION" ] || { echo "Interpretter does not appear to be Bash, aborting." ;exit 1 ; }


## runtime options ##
while true ;do
	case "$1" in
		-f|--full-title)
			full_title_length_limit="$2"
			shift 2
			;;
		-F|--full-only)
			full_only="1"
			shift
			;;
		-h|-H|--help)
			_help ;exit 0
			;;
		-j|--jobs)
			jobs="$2"
			shift 2
			;;
		-l|--zoom-length)
			zoom_total="$2"
			shift 2
			;;
		-p|--parallel)
			bashver="0"
			shift
			;;
		-s|--zoom-start)
			zoom_start="$2"
			shift 2
			;;
		-z|--zoom-title)
			zoom_title_length_limit="$2"
			shift 2
			;;
		-Z|--zoom-only)
			zoom_only="1"
			shift
			;;
		--)
			break
			;;
		-?*)
			_error "Unknown option: '$1'" ;_help ;exit 1
			;;
		*)
			break
			;;
	esac
done


## options follow-up / etc ##
[[ $# -lt "1" ]] && { _error "Nothing to do, please specify at least one flac, wav, or mp3 file, or a folder containing same. Use the '--help' switch for more info." ;exit 1 ; }

[[ $full_only == "1" && $zoom_only == "1" ]] && unset 'full_only' 'zoom_only'

if [[ -z "$homebrew_bin" ]] ;then
	if [[ -x /usr/local/bin/brew ]] ;then
		homebrew_bin="/usr/local/bin"
	elif [[ -x /opt/homebrew/bin/brew ]] ;then
		homebrew_bin="/opt/homebrew/bin"
	fi
else
	[[ -x "$homebrew_bin"/brew ]] || { _error "Homebrew binary not found in user-configured homebrew_bin ($homebrew_bin), aborting" ;exit 1 ; }
fi
[[ -n $homebrew_bin ]] && PATH="$homebrew_bin":$PATH

[ "$jobs" = "0" ] && {
	# https://unix.stackexchange.com/a/564512 | https://gist.github.com/jj1bdx/5746298
	jobs="$( getconf _NPROCESSORS_ONLN 2> /dev/null )" || # Linux and similar...
		jobs="$( getconf NPROCESSORS_ONLN 2> /dev/null )" || # FreeBSD (and derivatives), OpenBSD, MacOS and similar...
		jobs="$( ksh93 -c 'getconf NPROCESSORS_ONLN' 2> /dev/null )" || # Solaris and similar...
		{ echo "Unable to detect available cores, using single-thread." ;jobs="1" ; }
}


## file/folder arguments ##
shopt -s nullglob nocaseglob nocasematch
for arg in "$@" ;do
	if [[ -d $arg ]] ;then
		sources+=( "$arg"/*.flac "$arg"/*.wav "$arg"/*.mp3 )
	else
		case $arg in
			*.flac|*.wav|*.mp3)
				sources+=( "$arg" )
				;;
		esac
	fi
done
shopt -u nullglob nocaseglob nocasematch
for source_file in "${sources[@]}" ;do
	# handle duplicate entries? add realpath depend? just check (in _spectrograms) for existing spectrogram and skip? add option to skip or clobber existing?
	# short of adding a realpath depend for 'sort -u' ... what is the need for absolute paths here?
	if [[ ${source_file:0:1} = "/" ]] ;then absolute_sources+=( "$source_file" ) ;else absolute_sources+=( "${PWD}/${source_file}" ) ;fi
done
[[ ${#absolute_sources[@]} -lt "1" ]] && { _error "No flac, wav, or mp3 files files found, aborting. Use '--help' switch for more info." ;exit 1 ; }


## making arrays
for index in "${!absolute_sources[@]}" ;do
	absolute_source_dirs[$index]="$( dirname    "${absolute_sources[$index]}" )"
	    source_filenames[$index]="$( basename   "${absolute_sources[$index]}" )"
	   source_bit_depths[$index]="$( sox --i -b "${absolute_sources[$index]}" )" ## shows "0" for mp3, which is valid, but by itself is not great output
	 source_sample_rates[$index]="$( sox --i -r "${absolute_sources[$index]}" )"
	    source_durations[$index]="$( sox --i -D "${absolute_sources[$index]}" )"
done


## run with parallel when forced, or use 'wait' / 'wait -n'
if { [ "$bashver" = "0" ] && [ "$jobs" -gt "1" ] ; } ;then
	if epb="$( command -v env_parallel.bash )" ;then
		source "$epb"
		env_parallel --will-cite -j "$jobs" _spectrograms ::: "${!absolute_sources[@]}"
	else
		printf 'env_parallel not found, aborting. Try running without the "-p" option.\n'
		exit 1
	fi
else
	if { { [ "${BASH_VERSINFO[0]}" -eq 4 ] && [ "${BASH_VERSINFO[1]}" -ge 3 ] ; } || [ "${BASH_VERSINFO[0]}" -gt 4 ] ; } ;then
		bashver="4"
	elif [ "${BASH_VERSINFO[0]}" -le 3 ] ;then
		bashver="3"
	fi

	i="0"
	for index in "${!absolute_sources[@]}" ;do

		[ "$i" -ge "$jobs" ] && {
			case $bashver in
				4)
					wait -n
					;;
				3)
					wait
					i="0"
					;;
			esac
		}

		_spectrograms "$index" &
		i="$(( i + 1 ))"

	done
	wait
fi
