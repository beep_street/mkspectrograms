#### Name:
mkspectrograms - Make spectrograms of FLAC, WAV, and MP3 files.

#### Synopsis:
`mkspectrograms.sh [OPTION [ARGUMENT]]... [--] FLAC_WAV_MP3_OR_FOLDER [FLAC_WAV_MP3_OR_FOLDER]...`

#### Description:
A multi-process sox frontend for creating spectrograms of FLAC, WAV, and MP3 files.

#### Depends:
`bash, sox, basename, dirname`

#### Recommends:
`bash v4.3+` - Eariler versions are also supported, but v4.3 and newer support using `wait -n` for slightly more efficient multiprocess operation (per-file vs per-set-of-files).

#### Suggests:
`env_parallel` (part of GNU Parallel) - Remains available as an optional multiprocess fallback.

#### Documentation:
`-h | -H | --help` Print full help text.
